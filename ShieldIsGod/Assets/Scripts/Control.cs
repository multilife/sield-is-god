﻿// Control.cs
// copyright Kazuki_FUKUNAGA 2016 un-Pro.

using UnityEngine;
using UnityEngine.UI;
using System.Collections;
// using System.Collections.Generic;

public class Control : MonoBehaviour {

	[SerializeField]
	Image baseImage;
	[SerializeField]
	Image handleImage;
	[SerializeField]
	Image shieldControlImage;

	Color baseBaseColor;
	Color baseHandleColor;
	[SerializeField]
	Color shieldedColor;

	void Awake ()
	{
	}

	void Start ()
	{
		baseBaseColor = baseImage.color;
		baseHandleColor = handleImage.color;
	}
	
	void Update ()
	{
		baseImage.gameObject.SetActive(TouchSystem.onFirstTouch);
		shieldControlImage.gameObject.SetActive(TouchSystem.onSecondTouch);

		if (TouchSystem.onFirstTouch)
		{
			baseImage.GetComponent<RectTransform>().position = TouchSystem.firstTouchBeganPos;
			handleImage.GetComponent<RectTransform>().position = TouchSystem.firstTouchBeganPos + TouchSystem.firstTouchPos;

			baseImage.color = (TouchSystem.onSecondTouch? shieldedColor: baseBaseColor);
			handleImage.color = (TouchSystem.onSecondTouch? shieldedColor: baseHandleColor);

			if (TouchSystem.onSecondTouch)
			{
				shieldControlImage.GetComponent<RectTransform>().position = TouchSystem.secondTouchBeganPos + TouchSystem.secondTouchPos;
			}
		}
	}
}
