﻿// Score.cs
// copyright Kazuki_FUKUNAGA 2016 un-Pro.

using UnityEngine;
// using UnityEngine.UI;
using System.Collections;
// using System.Collections.Generic;

public class Score : MonoBehaviour
{
    // スコアを表示するGUIText
    public GUIText scoreGUIText;

    // ハイスコアを表示するGUIText
    public GUIText highScoreGUIText;

    // スコア
    private float score = 0;

    // ハイスコア
    private int highScore;

	[SerializeField]
	float scoreParTime = 1;

	public bool isGame = false;

    // PlayerPrefsで保存するためのキー
    private string highScoreKey = "highScore";

    void Start ()
    {
        Initialize ();
    }

    void Update ()
    {
        // スコアがハイスコアより大きければ
        if (highScore < score) {
			highScore = Mathf.FloorToInt(score);
        }

		if (isGame)
			score += Time.deltaTime * scoreParTime;

        // スコア・ハイスコアを表示する
        scoreGUIText.text = Mathf.FloorToInt(score).ToString ();
		highScoreGUIText.text = highScore.ToString ();
        // highScoreGUIText.text = "HighScore : " + highScore.ToString ();
    }

    // ゲーム開始前の状態に戻す
    private void Initialize ()
    {
        // スコアを0に戻す
        score = 0;

		isGame = false;

        // ハイスコアを取得する。保存されてなければ0を取得する。
        highScore = PlayerPrefs.GetInt (highScoreKey, 0);
    }

    // ポイントの追加
    public void AddPoint (int point)
    {
        score = score + point;
    }

    // ハイスコアの保存
    public void Save ()
    {
        // ハイスコアを保存する
        PlayerPrefs.SetInt (highScoreKey, highScore);
        PlayerPrefs.Save ();

        // ゲーム開始前の状態に戻す
        Initialize ();
    }
}