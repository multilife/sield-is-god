﻿// DestroyArea.cs
// copyright Kazuki_FUKUNAGA 2016 un-Pro.

using UnityEngine;
// using UnityEngine.UI;
using System.Collections;
// using System.Collections.Generic;

public class DestroyArea : MonoBehaviour
{
    void OnTriggerExit2D (Collider2D c)
    {
        Destroy (c.gameObject);
    }
}