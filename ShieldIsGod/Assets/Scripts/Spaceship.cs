﻿// SpaceShip.cs
// copyright Kazuki_FUKUNAGA 2016 un-Pro.

using UnityEngine;
// using UnityEngine.UI;
using System.Collections;
// using System.Collections.Generic;

// Rigidbody2Dコンポーネントを必須にする
[RequireComponent(typeof(Rigidbody2D))]
public class Spaceship : MonoBehaviour
{
    // 移動スピード
    public float speed;

    // 弾を撃つ間隔
    public float shotDelay;

    // 弾のPrefab
    public GameObject bullet;

    // 弾を撃つかどうか
    public bool canShot;

    // 爆発のPrefab
    public GameObject explosion;

    // アニメーターコンポーネント
    private Animator animator;

    void Start ()
    {
        // アニメーターコンポーネントを取得
        animator = GetComponent<Animator> ();
    }

    void Update()
    {
        Vector3 vpp = Camera.main.WorldToViewportPoint(this.transform.position);
        if (vpp.y < -0.3f)
        {
            Destroy(this.gameObject);
        }
    }

    // 爆発の作成
    public void Explosion ()
    {
        Instantiate (explosion, transform.position, transform.rotation);
    }

    // 弾の作成
    public void Shot (Transform origin)
    {
        GameObject go = (GameObject)Instantiate (bullet, origin.position, origin.rotation);
        go.transform.parent = this.transform.parent;
    }

    // アニメーターコンポーネントの取得
    public Animator GetAnimator()
    {
        return animator;
    }
}