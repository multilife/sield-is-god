﻿// Player.cs
// copyright Kazuki_FUKUNAGA 2016 un-Pro.

using UnityEngine;
using UnityEngine.UI;
using System.Collections;
// using System.Collections.Generic;

public class Player : MonoBehaviour
{
    // Spaceshipコンポーネント
    Spaceship spaceship;
    // シールドバーの塗りつぶしのやつ
    RectTransform shieldBar;
    // シールドスケールバーの塗りつぶしのやつ
    RectTransform shieldScaleBar;

    public float shieldPower; // シールドパワー（消えるまでの秒数）
    [SerializeField] // ←publicじゃなくても、次の行の変数をInspectorタブに表示できる。
    float shieldRepair = 1f; // シールド使ってない時の回復量/秒
    [SerializeField]
    float maxShieldPower = 10f; // シールドの最大値(持続秒)
    [SerializeField]
    float maxShieldScale = 3f; // 最大値の時のシールドの大きさ
    float shieldMultip = 1.0f; // シールドの大きさ操作
    [SerializeField]
    MinMax shieldMultipMinMax; // シールドの大きさ操作の範囲
    [SerializeField]
    GameObject shieldObject;

    Image bar; // バーのFillを取得。ImageクラスはUnityEngine.UIをusingする必要がある。
    Color defaultBarColor; // Inspectorタブ内で設定した色を保存しておく。警告色から色を戻す時用。

    [SerializeField]
    AnimationCurve ControlDistanceToSpeed; // コントローラーの距離を移動速度に変換するときの相関グラフ

    void Start () // ← yield return をコメントアウトしたのでIEnumeratorを返さなくなった。→何も返さないのでvoidに。
    // IEnumerator Start ()
    {
        // Spaceshipコンポーネントを取得
        spaceship = GetComponent<Spaceship> ();

        // while (true) {

        //     // 弾をプレイヤーと同じ位置/角度で作成
        //     spaceship.Shot (transform);

        //     // ショット音を鳴らす
        //     GetComponent<AudioSource>().Play();

        //     // shotDelay秒待つ
        //     yield return new WaitForSeconds (spaceship.shotDelay);
        // }

        shieldPower = maxShieldPower;

        shieldBar = GameObject.Find("ShieldBar").transform.FindChild("Fill").GetComponent<RectTransform>();
        shieldScaleBar = GameObject.Find("ShieldScaleBar").transform.FindChild("Fill").GetComponent<RectTransform>();

        bar = GameObject.Find("ShieldBar").transform.FindChild("Fill").GetComponent<Image>();
        defaultBarColor = bar.color;
    }

    GameObject shield;
    void Update ()
    {
        
        // 移動する向きを求める
        // Vector2 direction = new Vector2 (x, y).normalized;
        // Vector2 direction = TouchSystem.firstTouchPos.normalized * Mathf.Min(100, TouchSystem.firstTouchPos.magnitude) / 100f;
        // Vector2 direction = TouchSystem.firstTouchPos.normalized * ControlDistanceToSpeed.Evaluate(TouchSystem.firstTouchPos.magnitude);
        Vector2 direction = TouchSystem.firstTouchSpeed.normalized * Mathf.Min(100, TouchSystem.firstTouchSpeed.magnitude) * -1 / 15;

        // 移動の制限
        Move (direction);



        /* = = = = = = = = = = ここからシールドの話。 = = = = = = = = = = */

        // if (Input.GetKey(KeyCode.Space))
        // if (Manager.touched)
        if (TouchSystem.onSecondTouch)
        { // スペース押してる間

            // シールドの大きさ調整の速度
            const float shieldControlSpeed = 2;
            // シールドの大きさ調整のドヤコヤ
            shieldMultip = shieldMultipMinMax.BeBetoween( shieldMultip - TouchSystem.secondTouchSpeed.y * (shieldMultipMinMax.Gap * shieldControlSpeed / (float)Screen.height) );

            if (!(shield = GameObject.Find("Shield")))
            { // シールドが存在しなければ
                // Debug.Log("シールド作るよ！");
                shield = Instantiate(shieldObject);
                shield.name = "Shield";
                shield.GetComponent<Shield>().player = this;
                shield.transform.position = transform.position;
            }

            { // シールドの常用の挙動

                if (shieldPower <= 0)
                { // ShieldPower が0になった時
                    Dead();
                    return;
                }
                else // ShieldPower が0じゃなかったら減らしていく
                    shieldPower -= Time.deltaTime * shieldMultip;
            }

        }
        else 
        {
            if (shield = GameObject.Find("Shield"))
            { // スペース押してなくて、シールドが存在したら
                // Debug.Log("シールド消すよ！");
                Destroy(shield.gameObject);
                shield = null;
            }

            { // シールド出てない時の処理 

                // shieldPowerの回復。
                shieldPower = Mathf.Min(shieldPower + shieldRepair, maxShieldPower);

            }
        }

        if (shield)
            // シールドのスケールをshieldPowerとshieldMultipに応じて変える。
            shield.transform.localScale = Vector3.one * (maxShieldScale * (shieldPower / maxShieldPower) * shieldMultip + 0.1f);
        // シールドバーの大きさを制御する。
        shieldBar.localScale = new Vector3((shieldPower / maxShieldPower), 1, 1);
        shieldScaleBar.localScale = new Vector3(shieldMultipMinMax.GetRate(shieldMultip), 1, 1);


        if (bar.GetComponent<RectTransform>().localScale.x < 0.1f)
        { // shieldPower が10%を下回ったらパワーバーを赤色にする
            bar.color = new Color(255, 0, 0, 0.5f);
        }
        else if (bar.color != defaultBarColor)
        {
            bar.color = defaultBarColor;
        }

    }

    // 機体の移動
    void Move (Vector2 direction)
    {
        // 画面左下のワールド座標をビューポートから取得
        Vector2 min = Camera.main.ViewportToWorldPoint(new Vector2(0, 0));

        // 画面右上のワールド座標をビューポートから取得
        Vector2 max = Camera.main.ViewportToWorldPoint(new Vector2(1, 1));

        // プレイヤーの座標を取得
        Vector2 pos = transform.position;

        // 移動量を加える
        pos += direction  * spaceship.speed * Time.deltaTime;

        // プレイヤーの位置が画面内に収まるように制限をかける
        pos.x = Mathf.Clamp (pos.x, min.x, max.x);
        pos.y = Mathf.Clamp (pos.y, min.y, max.y);

        // 制限をかけた値をプレイヤーの位置とする
        transform.position = pos;
    }

    // ぶつかった瞬間に呼び出される
    void OnTriggerEnter2D (Collider2D c)
    {
        // レイヤー名を取得
        string layerName = LayerMask.LayerToName(c.gameObject.layer);

        // レイヤー名がBullet (Enemy)の時は弾を削除
        if( layerName == "Bullet (Enemy)")
        {
            // 弾の削除
			Destroy(c.gameObject);
        }

        // レイヤー名がBullet (Enemy)またはEnemyの場合は爆発
        if( layerName == "Bullet (Enemy)" || layerName == "Enemy")
        {
            Dead();
        }
    }

    void Dead()
    {
        GameObject shield;
        if (shield = GameObject.Find("Shield"))
        { // シールドが存在したら消す。
            Destroy(shield);
        }

        // Managerコンポーネントをシーン内から探して取得し、GameOverメソッドを呼び出す
        FindObjectOfType<Manager>().GameOver();

        // 爆発する
        spaceship.Explosion();

        // shieldPowerバーの色を戻す
        bar.color = defaultBarColor;

        // プレイヤーを削除
        Destroy (gameObject);
    }
}