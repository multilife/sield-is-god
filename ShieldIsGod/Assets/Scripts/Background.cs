﻿// Background.cs
// copyright Kazuki_FUKUNAGA 2016 un-Pro.

using UnityEngine;
// using UnityEngine.UI;
using System.Collections;
// using System.Collections.Generic;

public class Background : MonoBehaviour
{
    // スクロールするスピード
    public float speed = 0.1f;

    void Start()
    {
        // Debug.Log(Camera.main.pixelWidth / 640f);
        this.transform.localScale = new Vector3(8f* (Camera.main.pixelWidth / 640f) / (Camera.main.pixelHeight / 960f), 6f, 1f);
        // this.transform.localScale = new Vector3(8f* Camera.main.pixelWidth / 640f, 6f* Camera.main.pixelHeight / 960f, 1f);
        // Debug.Log(this.transform.localScale.x);
        this.GetComponent<MeshRenderer>().material.mainTextureScale = new Vector2(1, 2f* (Camera.main.pixelHeight / 960f) / (Camera.main.pixelWidth / 640f));
        // Debug.Log(this.GetComponent<MeshRenderer>().material.mainTextureScale.y);
    }

    void Update ()
    {
        // 時間によってYの値が0から1に変化していく。1になったら0に戻り、繰り返す。
        float y = Mathf.Repeat (Time.time * speed, 1);

        // Yの値がずれていくオフセットを作成
        Vector2 offset = new Vector2 (0, y);

        // マテリアルにオフセットを設定する
        GetComponent<Renderer>().sharedMaterial.SetTextureOffset ("_MainTex", offset);
    }
}