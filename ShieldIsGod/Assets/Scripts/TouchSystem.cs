﻿// TouchSystem.cs
// copyright Kazuki_FUKUNAGA 2016 un-Pro.

using UnityEngine;
// using UnityEngine.UI;
using System.Collections;
// using System.Collections.Generic;

public class TouchSystem : MonoBehaviour {

	[SerializeField]
	bool debug = false;

	static public bool onFirstTouch = false;
	static public bool onSecondTouch = false;
	static public Vector2 firstTouchBeganPos = Vector2.zero;
	static public Vector2 firstTouchPos = Vector2.zero;
	Vector2 firstTouchLastPos = Vector2.zero;
	static public Vector2 firstTouchSpeed = Vector2.zero;
	static public Vector2 secondTouchBeganPos = Vector2.zero;
	static public Vector2 secondTouchPos = Vector2.zero;
	Vector2 secondTouchLastPos = Vector2.zero;
	static public Vector2 secondTouchSpeed = Vector2.zero;

	void Awake ()
	{
	}

	void Start ()
	{
	}
	
	void Update ()
	{
		if (debug)
		{
			MouseDebug();
			return;
		}

		// Debug.Log(Input.touchCount +" :指の数");
		// 1本以上の指を感知してたら実行。onFirstTouchがTrueになる。
		if (onFirstTouch = Input.touchCount > 0)
		{

			// 1本目のタッチの情報を取得。(毎フレームの指のトラッキングはスマホ側が自動でやってくれる。ヤッター)
			Touch touch = Input.GetTouch(0);

			// Debug.Log(touch.phase +" :タッチの状態を確認");

			switch (touch.phase)
			{
				case TouchPhase.Began: // 画面に指が触れた時
					firstTouchBeganPos = touch.position;
					firstTouchPos = Vector2.zero;
					firstTouchLastPos = firstTouchPos;
					firstTouchSpeed = Vector2.zero;
					break;

				case TouchPhase.Moved: // 画面上で指が動いたとき
				case TouchPhase.Stationary: // 指が画面に触れているが動いてはいないとき
					if (firstTouchBeganPos.magnitude == 0) 
					{
						firstTouchBeganPos = touch.position;
						firstTouchLastPos = firstTouchPos;
					}
					firstTouchPos = touch.position - firstTouchBeganPos;
					firstTouchSpeed = (firstTouchLastPos - firstTouchPos);
					firstTouchLastPos = firstTouchPos;
					break;

				case TouchPhase.Ended: // 画面から指が離れたとき
				case TouchPhase.Canceled: // 認識できる指の数以上でタッチしたり、なんらかの状態でキャンセルになったとき
					firstTouchBeganPos = Vector2.zero;
					firstTouchPos = Vector2.zero;
					firstTouchSpeed = Vector2.zero;
					break;
			}
		}

		// 2本以上の指を感知してたら実行。onSecondTouchがTrueになる。
		if (onSecondTouch = (Input.touchCount > 1))
		{
			Touch touch2 = Input.GetTouch(1);

			switch (touch2.phase)
			{
				case TouchPhase.Began: // 画面に指が触れた時
					secondTouchBeganPos = touch2.position;
					secondTouchPos = Vector2.zero;
					secondTouchLastPos = secondTouchPos;
					secondTouchSpeed = Vector2.zero;
					break;

				case TouchPhase.Moved: // 画面上で指が動いたとき
				case TouchPhase.Stationary: // 指が画面に触れているが動いてはいないとき
					if (secondTouchBeganPos.magnitude == 0) {
						secondTouchBeganPos = touch2.position;
						secondTouchLastPos = secondTouchPos;
					}
					secondTouchPos = touch2.position - secondTouchBeganPos;
					secondTouchSpeed = (secondTouchLastPos - secondTouchPos);
					secondTouchLastPos = secondTouchPos;
					break;

				case TouchPhase.Ended: // 画面から指が離れたとき
				case TouchPhase.Canceled: // 認識できる指の数以上でタッチしたり、なんらかの状態でキャンセルになったとき
					secondTouchBeganPos = Vector2.zero;
					secondTouchPos = Vector2.zero;
					secondTouchSpeed = Vector2.zero;
					break;
			}


		}
	}

	void MouseDebug()
	{
		if (Input.touchCount > 0) debug = false;

		onFirstTouch = Input.GetMouseButton(0);
		onSecondTouch = Input.GetKey(KeyCode.Space);

		if (Input.GetMouseButtonDown(0))
		{
			// Debug.Log("マウスクリック！");

			firstTouchBeganPos = Input.mousePosition;
			firstTouchPos = Vector2.zero;
			firstTouchLastPos = firstTouchPos;
			firstTouchSpeed = Vector2.zero;
		}
		if (Input.GetMouseButton(0))
		{
			// Debug.Log("マウスクリック中");

			firstTouchPos = (Vector2)Input.mousePosition - firstTouchBeganPos;
			firstTouchSpeed = (firstTouchLastPos - firstTouchPos);
			firstTouchLastPos = firstTouchPos;
		}
		if (Input.GetMouseButtonUp(0))
		{
			// Debug.Log("マウスクリックアップ");

			firstTouchBeganPos = Vector2.zero;
			firstTouchPos = Vector2.zero;
			firstTouchSpeed = Vector2.zero;
		}

		// Debug.Log(firstTouchPos.magnitude);
	}
}
