﻿// Shield.cs
// copyright Kazuki_FUKUNAGA 2016 un-Pro.

using UnityEngine;
// using UnityEngine.UI;
using System.Collections;
// using System.Collections.Generic;

public class Shield : MonoBehaviour
{
	// プレイやーの取得を省略。Player型なのは、Player.cs内で「= this;」って代入できるように。
	public Player player;
	[SerializeField]
	int pointEnemyBullet = 30;

	void Awake()
	{
	
	}

	void Start ()
	{
		
	}
	
	void Update () 
	{
		// 追尾
		transform.position = player.transform.position;
	}

	void OnTriggerEnter2D(Collider2D col)
	{
        // レイヤー名を取得
        string layerName = LayerMask.LayerToName(col.gameObject.layer);

        // レイヤー名がBullet (Enemy)の時は弾を削除
        if( layerName == "Bullet (Enemy)")
        {
            // 弾の削除
			Destroy(col.gameObject);

			// スコアコンポーネントを取得してポイントを追加
			FindObjectOfType<Score>().AddPoint(pointEnemyBullet);
        }
	}
}