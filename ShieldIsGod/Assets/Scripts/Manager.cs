﻿// Manager.cs
// copyright Kazuki_FUKUNAGA 2016 un-Pro.

using UnityEngine;
// using UnityEngine.UI;
using System.Collections;
// using System.Collections.Generic;

public class Manager : MonoBehaviour
{
    // Playerプレハブ
    public GameObject player;

    // シールドバー
    public GameObject[] mainGameObject;

    // タイトル
    private GameObject title;

    private GameObject touch;

    public static bool touched = false;
    public void setTouch(bool touch){touched = touch;}

    void Awake ()
    {
        // Titleゲームオブジェクトを検索し取得する
        title = GameObject.Find ("Title");
        touch = GameObject.Find("TouchEvent");
    }

    void Update ()
    {
        // ゲーム中ではなく、Xキーが押されたらtrueを返す。
        // if (IsPlaying () == false && Input.GetKeyDown (KeyCode.X)) {
        //     GameStart ();
        // }
    }

    public void GameStart ()
    {
        // ゲームスタート時に、タイトルを非表示にしてプレイヤーを作成する
        title.SetActive (false);
        foreach (GameObject go in mainGameObject)
        {
            go.SetActive (true);
        }
        
        Instantiate (player, player.transform.position, player.transform.rotation);

		GameObject.Find ("Score GUI").GetComponent<Score> ().isGame = true;
    }

    public void GameOver ()
    {
        // ハイスコアの保存
        FindObjectOfType<Score>().Save();

        // ゲームオーバー時に、タイトルを表示する
        title.SetActive (true);
        touch.SetActive(true);
    }

    public bool IsPlaying ()
    {
        // ゲーム中かどうかはタイトルの表示/非表示で判断する
        return title.activeSelf == false;
    }
}